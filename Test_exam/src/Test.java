class Test2{
    private static int x=0;

    public static int getX() {
        return x;
    }

    public static void setX(int x) {
        Test2.x = x;
    }
}
public class Test {
    public static void main(String[] args){
        for(int i=0; i<5;i++) {
            System.out.println("The value of X: " + Test2.getX());
            Test2.setX(i);
        }
    }
}
