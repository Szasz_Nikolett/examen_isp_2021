class Vehicle {

}

class Car extends Vehicle {
    private String color;
    private Wheel[] wheels = new Wheel[4];
    private Windshield windshield;

    public Car() {
        wheels[0] = new Wheel();
        wheels[1] = new Wheel();
        wheels[2] = new Wheel();
        wheels[3] = new Wheel();
        windshield = new Windshield();
    }

    public void start() {

    }

    public void stop() {

    }

    public void go() {

    }
}

class Wheel {

}

class Windshield {

}

class CarMaker {
    private Car car;
}

class User {
    public void func(Car car) {

    }
}
