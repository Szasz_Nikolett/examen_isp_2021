import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Sub2 extends JFrame {

    JTextField tField1, tField2;
    JButton submit;

    Sub2() {
        setTitle("Ex2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 400);
        setVisible(true);

    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;

        submit = new JButton("Submit");
        submit.setBounds(90, 300, width, height);

        submit.addActionListener(new TreatButton());

        tField1 = new JTextField();
        tField1.setBounds(10, 100, 250, 60);

        tField2 = new JTextField();
        tField2.setBounds(10, 200, 250, 60);
        tField2.setEditable(false);

        add(tField1);
        add(tField2);
        add(submit);
    }

    public static void main(String[] args) {
        new Sub2();
    }

    class TreatButton implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String line = tField1.getText();
            Path path = Paths.get("");
            String l = path.toAbsolutePath().toString() + "\\src/";
            line = l + line;
            int n = 0;
            String verif = line.substring(line.length() - 4);
            if (verif.equals(".txt")) {
                try {
                    BufferedReader br = new BufferedReader(new FileReader(line));
                    while ((line = br.readLine()) != null)
                        n += line.length();
                    br.close();


                } catch (IOException a) {
                    System.out.println("Error! If the file is not found, please reconsider adding the text file in the src folder!");
                    a.printStackTrace();
                }
                tField2.setText(String.valueOf(n));
            } else
                System.out.println("It is not a text file! Please don't forget to write the extension");


            tField1.setText("");


        }
    }
}


